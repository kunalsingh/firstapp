<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/update', 'UpdateController@index')->name('userprofileupdate');
Route::post('/profileupdate', 'UpdateController@profileupdate')->name('profileupdate');

Route::get('/employee/create',[
    'uses' =>'EmployeeController@create',
    'as'   =>'create'
    
        ]);

Route::get('/employee/search',[
    'uses' =>'EmployeeController@search',
    'as'   =>'search'
    
        ]);



Route::post('/employee/store',[
    'uses' =>'EmployeeController@store',
    'as'   =>'store'

   ]);

Route::post('/employee/update',[
    'uses' =>'EmployeeController@update',
    'as'   =>'update'

   ]);
Route::get('/employee/index',[
  'uses' =>'EmployeeController@index',
   'as'=>'index'
   ]);



Route::get('/employee/delete/{id}',[
  'uses' =>'EmployeeController@delete',
   'as'=>'delete'
   ]);

Route::get('/employee/edit/{id?}', array('as' => 'edit', 'uses' => 'EmployeeController@edit'));