@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                     <div class="card-body">
                        <a href="{{route('create')}}" class="btn btn-primary pull-right col-md-2" >Add New Employee</a>
                        <br><br>

                          <form method="GET" action="{{ route('index') }}">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" name="emp_name" class="form-control" placeholder="Search" value="{{old('emp_name',@$employee->emp_name)}}">
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="emp_dept">
                            <option value="">Select Department</option>
                            
                            @foreach($department as $departments)

                            
                              <option value="{{$departments->department}}" {{ $departments->department == @$employee->emp_dept  ? 'selected' : ''}}>{{$departments->department}}</option>
                            @endforeach
                          </select> 
                                </div>
                                <div class="col-md-4">
                                    <button class="btn btn-success">Search</button>
                                </div>
                            </div>
                        </form>

                   
                            <h1>Employee List</h1>
                            <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Dept</th>
                                        <th>Employee Salary</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($employee as $employees)
                                        <tr>
                                            <td>{{$employees->emp_name}}</td>  
                                            <td>{{$employees->emp_dept}}</td>
                                            <td>{{$employees->emp_salary}}</td>
                                            <td>
                                                <a href="{{URL::route('edit')}}/{{$employees -> id}}" class="btn btn-primary">Edit</a> 
                                                <a href="{{route('delete',['id' => $employees->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>

                            <?php echo $employee->render(); ?>
                     
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
