@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <h1>Employee Create</h1>
                    <form action="{{route('store')}}" method="post" >
                        <div class="form-group">
                        <label for="title">Employee Name</label>
                        <input type="text" name="emp_name" id="emp_name" class="form-control" value="{{old('title',@$employee->emp_name)}}" >
                        <div id="title-err" style="color:red;">{{$errors->first('emp_name')}}</div>
                        </div>

                        <div class="form-group">
                        <label for="title">Employee Dept</label>
                        <select class="form-control" name="emp_dept">
                            <option value="">Select Department</option>
                            @foreach($department as $departments)
                              <option value=" {{$departments->department}}">{{$departments->department}}</option>
                            @endforeach
                          </select> 
                        <div id="emp_dept-err" style="color:red;">{{$errors->first('emp_dept')}}</div>
                        </div>

                        <div class="form-group">
                        <label for="title">Employee Salary</label>
                        <input type="text" name="emp_salary" id="emp_salary" class="form-control" value="{{old('title',@$employee->emp_salary)}}" >
                        <div id="title-err" style="color:red;">{{$errors->first('emp_salary')}}</div>
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                        {{csrf_field()}}
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
