@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <h1>Todos</h1>
                    <form action="{{route('update')}}" method="post" >
                        <input type="hidden" id="id" name="id" value="<?php if(!empty(@$employee->id)) echo @$employee->id; ?>" > 
                        <div class="form-group">
                        <label for="title">Employee Name</label>
                        <input type="text" name="emp_name" id="emp_name" class="form-control" value="{{old('emp_name',@$employee->emp_name)}}" >
                        <div id="emp_name-err" style="color:red;">{{$errors->first('emp_name')}}</div>
                        </div>

                        <div class="form-group">
                        <label for="title">Employee Dept</label>
                        <select class="form-control" name="emp_dept">
                            <option value="">Select Department</option>
                            
                            @foreach($department as $departments)

                            
                              <option value="{{$departments->department}}" {{ $departments->department == @$employee->emp_dept  ? 'selected' : ''}}>{{$departments->department}}</option>
                            @endforeach
                          </select> 
                        <div id="emp_dept-err" style="color:red;">{{$errors->first('emp_dept')}}</div>
                        </div>

                        <div class="form-group">
                        <label for="title">Employee Salary</label>
                        <input type="text" name="emp_salary" id="emp_salary" class="form-control" value="{{old('emp_salary',@$employee->emp_salary)}}" >
                        <div id="emp_salary-err" style="color:red;">{{$errors->first('emp_salary')}}</div>
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                        {{csrf_field()}}
                    </form>      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
