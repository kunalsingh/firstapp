@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <h1>Highest Employee Salary</h1>
                            <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Dept</th>
                                        <th>Employee Salary</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($highestSalary as $employees)
                                        <tr>
                                            <td>{{$employees->emp_name}}</td>  
                                            <td>{{$employees->emp_dept}}</td>
                                            <td>{{$employees->emp_salary}}</td>
                                            <td>
                                                <a href="{{URL::route('edit')}}/{{$employees -> id}}" class="btn btn-primary">Edit</a> 
                                                <a href="{{route('delete',['id' => $employees->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                            <br><br><br>

                        <h1>Lowest Employee Salary</h1>
                            <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Dept</th>
                                        <th>Employee Salary</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($lowestSalary as $employees)
                                        <tr>
                                            <td>{{$employees->emp_name}}</td>  
                                            <td>{{$employees->emp_dept}}</td>
                                            <td>{{$employees->emp_salary}}</td>
                                            <td>
                                                <a href="{{URL::route('edit')}}/{{$employees -> id}}" class="btn btn-primary">Edit</a> 
                                                <a href="{{route('delete',['id' => $employees->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                            <br><br><br>


                            <h1>Top Three Employee Salary</h1>
                            <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Dept</th>
                                        <th>Employee Salary</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($TopThreeSalary as $employees)
                                        <tr>
                                            <td>{{$employees->emp_name}}</td>  
                                            <td>{{$employees->emp_dept}}</td>
                                            <td>{{$employees->emp_salary}}</td>
                                            <td>
                                                <a href="{{URL::route('edit')}}/{{$employees -> id}}" class="btn btn-primary">Edit</a> 
                                                <a href="{{route('delete',['id' => $employees->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                            <br><br><br>



                             <h1>Less Three Employee Salary</h1>
                            <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Dept</th>
                                        <th>Employee Salary</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($BelowThreeSalary as $employees)
                                        <tr>
                                            <td>{{$employees->emp_name}}</td>  
                                            <td>{{$employees->emp_dept}}</td>
                                            <td>{{$employees->emp_salary}}</td>
                                            <td>
                                                <a href="{{URL::route('edit')}}/{{$employees -> id}}" class="btn btn-primary">Edit</a> 
                                                <a href="{{route('delete',['id' => $employees->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                            <br><br><br>


                             <h1>Less Salary Of Each DepartMent</h1>
                            <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Dept</th>
                                        <th>Employee Salary</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($lesssalaryofdepart as $employees)
                                        <tr>
                                            <td>{{$employees->emp_name}}</td>  
                                            <td>{{$employees->emp_dept}}</td>
                                            <td>{{$employees->emp_salary}}</td>
                                            <td>
                                                <a href="{{URL::route('edit')}}/{{$employees -> id}}" class="btn btn-primary">Edit</a> 
                                                <a href="{{route('delete',['id' => $employees->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                            <br><br><br>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection