<?php

namespace App\Http\Controllers;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$department =DB::table('departments')->get();
    	if($request->has('emp_name')){
    		$employee = Employee::where(['emp_name'=>$request->get('emp_name')])->Orwhere(['emp_dept'=>$request->get('emp_dept')])->paginate(5);	
    	}else{
        $employee= Employee::orderBy('id','DESC')->paginate(5);
    }

        return view('employee.index',compact('employee','department'));
    }

    public function search(Request $request){

    		$this->validate($request, [
            'emp_name' => 'required|string'
             
        ]);

    		$name= $request->get('emp_name');
    		$employee= Employee::where('emp_name',$name)->orderBy('id','DESC')->paginate(5);
    		return view('employee.index',compact('employee'));

    }
    

    public function create()
    {
    	$department =DB::table('departments')->get();
        return view('employee.create',['department'=>$department]);
    }
    


    public function store(Request $request)
    {
        $this->validate($request, [
            'emp_name' => 'required|string',
            'emp_dept' => 'required|string',
            'emp_salary'  => 'required|integer'
            
        ]);

        Employee::create($request->all());
        return redirect()->route('index')
                        ->with('success',' added successfully');
    }
   

    public function edit($id)
    {
        $employee= Employee::find($id);
        $department =DB::table('departments')->get();
        if(isset($employee)){
            return view('employee.edit',compact('employee','department'));
        }else{
            return redirect()->route('index');
        }
    }
    


    public function update(Request $request)
    {
        $id = $request['id'];
        $this->validate($request, [
            'emp_name' => 'required|string',
            'emp_dept' => 'required|string',
            'emp_salary'  => 'required|integer'
            
        ]);

        Employee::find($id)->update($request->all());
        return redirect()->route('index')
                        ->with('success','updated successfully');
    }
    


    public function delete($id)
    {
        Employee::find($id)->delete();
        return redirect()->route('index')
                        ->with('success','deleted successfully');
    }
}
