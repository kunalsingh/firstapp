<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UpdateController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $email = \Auth::user()->email;
         $user_id =\Auth::user()->id;
         $user = User::where(['email'=>$email])->first();

         $checkUser = Profile::where(['user_id'=>$user_id])->first();

         return view('user.update',['user'=>$user,'checkUser'=>$checkUser]);
    }

    public function profileupdate(Request $request)
    {
        //user data 

        $email = \Auth::user()->email;
        $user = User::where(['email'=>$email])->first();
        $profile=[];
        $request->validate([
            'email'=>'required',
            // 'profile_image' => 'required|file|max:1024',
        ]);
        

        $user_id = \Auth::user()->id;
        $file = $request->file('profile_image');
        
        if(isset($file)) {
        //Display File Name
        $filename =time().$file->getClientOriginalName();
       
        //Move Uploaded File
        $destinationPath = 'uploads';
        $file->move($destinationPath, $filename);

        //check user profile pic is uploaded 

        $checkUser = Profile::where(['user_id'=>$user_id])->first();
        
        if($checkUser){
           $profile = Profile::where(['user_id'=>$user_id])->update(['profile_image' =>$filename]);
        }
        else{
         $profile =Profile::create([
            'user_id'=> $user_id,
            // 'email' => $$request['email'],
            'profile_image' =>$filename,
        ]);
         }
        }

        if($request->new_password) {
        $id = \Auth::user()->id;
        $user = User::findOrFail($id);
       
        if (Hash::check($request->current_password, $user->password)) { 
  
        User::find(auth()->user()->id)->update(['password'=> \Hash::make($request->new_password)]);
        return redirect()->back()->with('success', 'Password changed');   

        } else {
            return redirect()->back()->with('error', 'Password doesnot changed');  
        }

    }
        return redirect('update');
        //  return view('home',['user'=>$user,'profile'=>$profile,'checkUser'=>$checkUser]);
    }
}
