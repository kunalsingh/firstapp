<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get highest salary
        $HighestSalary = DB::table('employees')
                ->orderBy('emp_salary','DESC')
                ->offset(0)
                ->limit(1)
                ->get();

        //Top Three highest salary
        $TopThreeSalary = DB::table('employees')
                ->orderBy('emp_salary','DESC')
                ->offset(0,3)
                ->limit(3)
                ->get();


        //Top Three highest salary
        $BelowThreeSalary = DB::table('employees')
                ->orderBy('emp_salary','ASC')
                ->offset(0,3)
                ->limit(3)
                ->get();



        //get Lowest salary
        $LowestSalary = DB::table('employees')
                ->orderBy('emp_salary','ASC')
                ->offset(0)
                ->limit(1)
                ->get();

        //less salary of each department employee
        $lesssalaryofdepart = DB::select( DB::raw("
                                               SELECT * FROM employees 
                                                where 
                                                emp_salary in (
                                                select min(emp_salary)
                                                from 
                                                employees
                                                group by 
                                                emp_dept
                                                )")
                                         );




         return view('home',['highestSalary'=>$HighestSalary,'lowestSalary'=>$LowestSalary,'TopThreeSalary'=>$TopThreeSalary,'BelowThreeSalary'=>$BelowThreeSalary ,'lesssalaryofdepart'=>$lesssalaryofdepart]);
    }
    
}
