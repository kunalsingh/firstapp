<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'github' => [
        'client_id' => '',         // Your GitHub Client ID
        'client_secret' => '', // Your GitHub Client Secret
        'redirect' => '',
    ],

    'facebook' => [
        'client_id' => '2536946279892537',         // Your Facebook Client ID
        'client_secret' => '314adb2609b20237eb702c8124f63a6c', // Your Facebook Client Secret
        'redirect' => 'https://localhost/firstapp/home',
    ],

    'google' => [
        'client_id' => '',         // Your Google Client ID
        'client_secret' => '', // Your Google Client Secret
        'redirect' => '',
    ],

];
