<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'department' => 'it'
            
        ]);

        DB::table('departments')->insert([
            'department' => 'manager'
            
        ]);

        DB::table('departments')->insert([
            'department' => 'hr'
            
        ]);

        DB::table('departments')->insert([
            'department' => 'scientist'
            
        ]);
    }
}
